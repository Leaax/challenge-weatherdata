package test;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import programm.*;

/**
 * Example JUnit4 test case.
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
public class AppTest {

    @Test
    public void testCalculateDifference() {
    	assertTrue(App.calculateDifference(15,5) == 10);
    	assertTrue(App.calculateDifference(5, 15) == 10);
    	assertTrue(App.calculateDifference(10, 10) == 0);
    }
    
    @Test
    public void testCalculateMin() {
    	Wrapper w1 = new Wrapper("1", 100, 45);
    	Wrapper w2 = new Wrapper("2", 89, 10);
    	Wrapper w3 = new Wrapper("3", 70, 31);
    	LinkedList<Wrapper> list = new LinkedList<Wrapper>();
    	list.add(w1);
    	list.add(w2);
    	list.add(w3);
    	assertTrue(App.calculateMin(list).getName().equals(w3.getName()));
    }
    
    @Test
    public void testProcessCsvLine() {
    	assertTrue(App.processCsvLine("3,77,55,66,39.6,0,350,5,350,9,2.8,59,24,1016.8", 1, 2).getName().equals("3"));
    	assertTrue(App.processCsvLine("3,77,55,66,39.6,0,350,5,350,9,2.8,59,24,1016.8", 1, 2).getDataOne() == 77);
    	assertTrue(App.processCsvLine("3,77,55,66,39.6,0,350,5,350,9,2.8,59,24,1016.8", 1, 2).getDataTwo() == 55);
    }
    
    @Test
    public void testProcessCsvFile() {
    	assertTrue(App.processCsvFile("./resources/weather.csv", 1, 2) != null);
    }

}