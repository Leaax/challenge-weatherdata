package programm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

/**
 * The entry class for your solution. This class is only aimed as starting point and not intended as baseline for your software
 * design. Read: create your own classes and packages as appropriate.
 *
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
public final class App {

    public static void main(String... args) {

        // Your preparation code …
    	String csvFileWeather = "./resources/weather.csv";
    	int weatherPosDataOne = 1;
    	int weatherPosDataTwo = 2;
    	String csvFileFootball = "./resources/football.csv";
    	int footballPosDataOne = 5;
    	int footballPosDataTwo = 6;

        String dayWithSmallestTempSpread = calculateMin(processCsvFile(csvFileWeather, weatherPosDataOne, weatherPosDataTwo)).getName();     // Your day analysis function call …
        String teamWithSmallesGoalSpread = calculateMin(processCsvFile(csvFileFootball, footballPosDataOne, footballPosDataTwo)).getName(); // Your goal analysis function call …

        System.out.printf("Day with smallest temperature spread : %s%n", dayWithSmallestTempSpread);
        System.out.printf("Team with smallest goal spread       : %s%n", teamWithSmallesGoalSpread);
    }
    
    /**
     * Reads the file and saves each lines data and name in a Wrapper Class
     * @param filePath
     * @return list with all Objects from the File
     */
    public static LinkedList<Wrapper> processCsvFile(String filePath, int posDataOne, int posDataTwo) {
    	//list setup
    	LinkedList<Wrapper> list = new LinkedList<Wrapper>(); //list of elements from the file
		try {
			//setup reader for file
	    	BufferedReader reader = new BufferedReader(new FileReader(filePath)); 
	        String current = reader.readLine(); // current line that gets proccesed
			//proccessing of each line and reading next line
			current = reader.readLine(); //again, because first line are headlines for each column
			while (current != null) { 
	            list.add(processCsvLine(current, posDataOne, posDataTwo)); 
	            current = reader.readLine(); 
	        } 
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return list;
    }
    
    /**
     * Processes each line, saves the name an data in a Wrapper Class
     * @param line
     * @param positionDataOne
     * @param positionDataTwo
     * @return wrapper
     */
    public static Wrapper processCsvLine(String line, int positionDataOne, int positionDataTwo) {
    	String[] splitLine = line.split(",");
    	String name = splitLine[0];
    	int dataOne = Integer.parseInt(splitLine[positionDataOne]);
    	int dataTwo = Integer.parseInt(splitLine[positionDataTwo]);
    	return new Wrapper(name,dataOne,dataTwo);
    }
    
    /**
     * Calculates the difference between two numbers
     * @param one
     * @param two
     * @return difference
     */
    public static int calculateDifference(int one, int two) {
    	int difference = 0;
    	if(one>two) {
    		difference = one - two;
    	}else if(two>one) {
    		difference = two - one;
    	}
    	return difference;
    }
    
    /**
     * calculates minimum of a list of Wrapper classes
     * @param list
     * @return the class with the lowest difference
     */
    public static Wrapper calculateMin(LinkedList<Wrapper> list) {
    	Wrapper min = list.getFirst();
    	int difference = calculateDifference(min.getDataOne(), min.getDataTwo());
    	for(int i = 0; i < list.size(); i++) {
    		if(difference > calculateDifference(list.get(i).getDataOne(), list.get(i).getDataTwo())) {
    			min = list.get(i);
    			difference = calculateDifference(list.get(i).getDataOne(), list.get(i).getDataTwo());
    		}
    	}
    	return min;
    }
}
