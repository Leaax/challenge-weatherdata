package programm;

/**
 * class that holds the data for the calculation
 * @author alexa
 *
 */
public class Wrapper {
	
	private String name; 
	private int dataOne;
	private int dataTwo;
	
	/**
	 * constructor for the class
	 * @param name
	 * @param dataOne
	 * @param dataTwo
	 */
	public Wrapper(String name, int dataOne, int dataTwo) {
		this.name = name;
		this.dataOne = dataOne;
		this.dataTwo = dataTwo;
	}
	
	/**
	 * getter for the name
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * getter for the first data
	 * @return dataOne
	 */
	public int getDataOne() {
		return dataOne;
	}

	/**
	 * getter for the second data
	 * @return dataTwo
	 */
	public int getDataTwo() {
		return dataTwo;
	}
}
